from pathlib import Path
from typing import Optional

import numpy as np
import pytest
from hamcrest.core import assert_that
from numpy.testing import assert_array_equal

from pilotis_io.exceptions import PilotisIoError
from pilotis_io.local import LocalIoApi, LocalNumpyApi
from pilotis_io.numpy import NumpyApi


@pytest.fixture
def numpy_api(tmp_path: Path) -> NumpyApi:
    io_api = LocalIoApi(project_dir=str(tmp_path))
    return LocalNumpyApi(io_api)


def test_save_numpy_array_in_npz_format(numpy_api: NumpyApi):
    # Given a numpy array
    arr = np.array([1, 2, 3, 4, 5])

    # When saving it as NPZ
    relative_export_path = Path("export.npz")
    numpy_api.save_numpy_array(arr, relative_export_path=relative_export_path)

    # Then it creates a file
    full_export_path = numpy_api.io_api.project_root_path / relative_export_path
    full_export_path.exists()
    # And the file content is readable using numpy lib
    loaded_arr = np.load(str(full_export_path))["arr_0"]
    assert_array_equal(arr, loaded_arr)


def test_save_numpy_array_in_unknown_format(numpy_api: NumpyApi):
    # Given a numpy array
    arr = np.array([1, 2, 3, 4, 5])

    # When saving it as an unknown format
    # Then it should raise an error
    relative_export_path = Path("export.unknown_format")
    with pytest.raises(PilotisIoError):
        numpy_api.save_numpy_array(arr, relative_export_path=relative_export_path)


def test_save_numpy_array_with_none_as_the_array(numpy_api: NumpyApi):
    # Given a numpy array
    arr: Optional[np.ndarray] = None

    # When saving it as an unknown format
    # Then it should raise an error
    relative_export_path = Path("export.unknown_format")
    with pytest.raises(PilotisIoError):
        numpy_api.save_numpy_array(arr, relative_export_path=relative_export_path)

    # And no file should be created
    full_path = numpy_api.io_api.project_root_path / relative_export_path
    assert_that(not full_path.exists())


def test_save_numpy_array_with_none_as_the_export_path(numpy_api: NumpyApi):
    # Given a numpy array
    arr = np.array([1, 2, 3, 4, 5])

    # When saving it as an unknown format
    # Then it should raise an error
    relative_export_path: Optional[Path] = None
    with pytest.raises(PilotisIoError):
        numpy_api.save_numpy_array(arr, relative_export_path=relative_export_path)


def test_load_numpy_array_with_a_npz_file(numpy_api: NumpyApi):
    # Given a numpy NPZ file inside the API's directory
    relative_path = Path("file.npz")
    full_path = numpy_api.io_api.project_root_path / relative_path
    arr = np.array([1, 2, 3, 4, 5])
    np.savez(str(full_path), arr)

    # When loading it
    loaded_arr = numpy_api.load_numpy_array(relative_path)

    # Then it should get the data in memory
    assert_array_equal(arr, loaded_arr)


def test_load_numpy_array_from_a_file_with_an_unknown_format(
    numpy_api: NumpyApi,
):
    # Given a numpy file with an unknown format inside the API's directory
    relative_path = Path("file.unknown_format")
    full_path = numpy_api.io_api.project_root_path / relative_path
    arr = np.array([1, 2, 3, 4, 5])
    np.savez(str(full_path), arr)

    # When loading it
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        numpy_api.load_numpy_array(relative_path)


def test_load_numpy_array_none_as_path(numpy_api: NumpyApi):
    # Given a None path
    relative_path: Optional[Path] = None

    # When loading it
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        numpy_api.load_numpy_array(relative_path)


def test_load_numpy_array_with_a_path_that_does_not_exists_should_throw_an_error(
    numpy_api: NumpyApi,
):
    # Given a None path
    relative_path = Path("file.npz")

    # When loading it
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        numpy_api.load_numpy_array(relative_path)
