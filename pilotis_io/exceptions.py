"""All package specific exceptions."""


class PilotisIoError(Exception):
    """
    Base exception for errors raised by pilotis-io.
    """
