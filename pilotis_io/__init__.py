"""Top-level package for Pilotis IO."""

__author__ = """ekinox"""
__email__ = "contact@ekinox.io"
from .__version__ import __version__  # noqa
